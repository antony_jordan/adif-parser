# Copyright (c) 2019 Antony Robert Jordan

"""
The heavy lifting part of the library. Uses the parsy library to
build components that get combined into a full ADIF2 parser.
"""

from typing import Union, List, Tuple, Dict, Generator

from parsy import regex, generate, string, fail, whitespace, Parser

from ADIF.types import DATATYPES, from_type, from_token, ADIF_TYPES

########################################################################################################################
# Type aliases to make type hinting readable
########################################################################################################################

TOKEN_TYPE = Tuple[str, int, Union[str, None]]
FIELD_TYPE = Tuple[str, ADIF_TYPES]
RECORD_TYPE = Dict[str, ADIF_TYPES]
HEADER_TYPE = Union[Tuple[Union[str, None], Union[RECORD_TYPE, None]], None]
FILE_TYPE = Dict[str, Union[HEADER_TYPE, List[RECORD_TYPE]]]


########################################################################################################################
# Parser Functions
########################################################################################################################


def _token_parser() -> Generator[Parser, str, TOKEN_TYPE]:
    """
    Parses a single ADIF2 token of the form ``<TOKEN:LENGTH:TYPE>``
    e.g: ``<QSO_DATE:8:d>``.

    The `TYPE` portion of the token is optional and can be omitted
    e.g: ``<QSO_DATE:8>``.

    Returns:
        - ``TOKEN`` as an uppercase string
        - ``LENGTH`` as an integer
        - ``TYPE`` as an uppercase character or `None` if it is not found

        All as a tuple: ``(TOKEN, LENGTH, TYPE)``

    Examples:
        >>> token_parser.parse('<TOKEN:42:S>')
        ('TOKEN', 42, 'S')

        >>> token_parser.parse('<TOKEN:42>')
        ('TOKEN', 42, None)

        >>> token_parser.parse('<TOKEN>')
        Traceback (most recent call last):
        ...
        parsy.ParseError: expected 'ADIF TOKEN' at 0:0

        >>> token_parser.parse('<TOKEN:S>')
        Traceback (most recent call last):
        ...
        parsy.ParseError: expected 'ADIF TOKEN' at 0:0
    """
    data_type = None

    yield string('<')
    token = yield regex(r'[a-zA-Z_]+')

    yield string(':')
    length = yield regex('[0-9]+')
    type_separator = yield string(':').optional()

    if type_separator is not None:
        data_type = yield regex(
            '({}|{})'.format('|'.join(DATATYPES.keys()), '|'.join(DATATYPES.keys()).lower()))
        data_type = data_type.strip().upper()

    yield string('>')

    return token.strip().upper(), int(length), data_type


def _field_parser() -> Generator[Parser, str, FIELD_TYPE]:
    """
    Parses a full ADIF2 field of the form `<TOKEN:LENGTH:TYPE>DATA`
    e.g: `<QSO_DATE:8:d>20110629`.

    The token portion (`<TOKEN:LENGTH:TYPE>`) of the field makes use of
    the `token_parser()` and therefore follows the same rules.

    Returns:
        - `TOKEN` as an uppercase string
        - `DATA` as an appropriate type dependant upon the the `TOKEN`
           and `TYPE` portions of the field determined by
           `ADIF.type.from_token()` and `ADIF.type.from_type()`

        All as a tuple: `(TOKEN, DATA)`

    Examples:

        >>> field_parser.parse('<QSO_DATE:8:d>20110629')
        ('QSO_DATE', datetime.date(2011, 6, 29))

        >>> field_parser.parse('<QSO_DATE:8>20110629')
        ('QSO_DATE', datetime.date(2011, 6, 29))
    """
    token, length, data_type = yield token_parser

    # Match any char other than < or > up to length amount of times
    raw_data = yield regex('[^<>]{{,{}}}'.format(length)).desc('ADIF DATA')
    raw_data = raw_data.strip()

    yield whitespace.optional()

    if data_type is not None:
        try:
            data = from_type(data_type, raw_data)
        except TypeError as e:
            return fail(e.args[0])
    else:
        try:
            data = from_token(token, raw_data)
        except TypeError as e:
            return fail(e.args[0])

    return token, data


def _header_parser() -> Generator[Parser, str, HEADER_TYPE]:
    """
    Parses an ADIF2 header.

    An ADIF2 header consists of any ASCII data excluding ``<`` or ``>``
    followed by a set of ADIF2 fields ending with ``<EOH>``.

    Returns:
        - ``DATA`` as a string of ASCII data from the begining of the header.
        - ``FIELDS`` as a dictionary of ``{TOKEN: FIELD_DATA}``.

        All as a tuple: ``(DATA, FIELDS)``

    Examples:

        >>> header_parser.parse('Some random text<TOKEN:4>Data<ANOTHER_TOKEN:5>Data2<EOH>')
        ('Some random text', {'TOKEN': 'Data', 'ANOTHER_TOKEN': 'Data2'})

        >>> header_parser.parse('Some random text<EOH>')
        ('Some random text', None)

        >>> header_parser.parse('<TOKEN:4>Data<ANOTHER_TOKEN:5>Data2<EOH>')
        (None, {'TOKEN': 'Data', 'ANOTHER_TOKEN': 'Data2'})

        >>> header_parser.parse('<EOH>')

    """
    data = None
    fields: Union[None, RECORD_TYPE] = None

    raw_data = yield regex('[^<>]').many().optional()

    if raw_data is not None and len(raw_data) > 0:
        data = (''.join(raw_data)).strip()

    yield whitespace.optional()

    raw_fields = yield field_parser.many().optional()

    if raw_fields is not None and len(raw_fields) > 0:
        fields = dict(raw_fields)

    yield whitespace.optional()
    yield string('<EOH>', transform=lambda s: s.upper()).desc('End of header token')

    if fields is not None or data is not None:
        return data, fields
    else:
        return None


def _record_parser() -> Generator[Parser, str, RECORD_TYPE]:
    """
    Parse an ADIF2 record.

    An ADIF2 record consists of a number of ADIF2 fields terminated at
    the end with a ``<EOR>`` token.

    Returns:
        A dictionary of fields in the form ``{TOKEN: DATA}``

    Examples:

        >>> record_parser.parse('<TOKEN:4>Data<EOR>')
        {'TOKEN': 'Data'}

        >>> record_parser.parse('<TOKEN:4>Data<ANOTHER_TOKEN:5>Data2<EOR>')
        {'TOKEN': 'Data', 'ANOTHER_TOKEN': 'Data2'}
    """
    yield whitespace.optional()
    data = yield field_parser.many()
    yield whitespace.optional()
    yield string('<EOR>', transform=lambda s: s.upper()).desc('End of record token')
    yield whitespace.optional()

    return dict(data)


def _file_parser() -> Generator[Parser, str, FILE_TYPE]:
    """
    Parses an ADIF2 file.

    An ADIF2 file consists of a header followed by any number
    of records.

    See ``_header_parser()`` and ``_record_parser()`` for
    details of what a header and record consist of.

    Returns:
        A dictionary with two keys:
          - ``header``: The header data as returned by
            ``_header_parser()``
          - ``records``: A list of ADIF2 records as returned by
            ``_record_parser()``

    Examples:
        file_parser.parse('Some random text<TOKEN:4>Data<ANOTHER_TOKEN:5>Data2<EOH><TOKEN:4>Data<ANOTHER_TOKEN:5>Data2<EOR><TOKEN:4>Data<ANOTHER_TOKEN:5>Data2<EOR>')
        {'header': ('Some random text', {'TOKEN': 'Data', 'ANOTHER_TOKEN': 'Data2'}), 'records': [{'TOKEN': 'Data', 'ANOTHER_TOKEN': 'Data2'}, {'TOKEN': 'Data', 'ANOTHER_TOKEN': 'Data2'}}
    """
    header = yield header_parser.optional()
    yield whitespace.optional()
    records = yield record_parser.many()
    yield whitespace.optional()

    return {'header': header, 'records': records}


########################################################################################################################
# Parsy Generator Functions - Done this way to allow documentation and tests to work correctly.
########################################################################################################################


@generate('ADIF TOKEN')
def token_parser() -> Generator[Parser, str, TOKEN_TYPE]:
    """ See ``_token_parser()`` """
    return _token_parser()


@generate('ADIF FIELD')
def field_parser() -> Generator[Parser, str, FIELD_TYPE]:
    """ See ``_field_parser()`` """
    return _field_parser()


@generate('ADIF HEADER')
def header_parser() -> Generator[Parser, str, HEADER_TYPE]:
    """ See ``_header_parser()`` """
    return _header_parser()


@generate('ADIF RECORD')
def record_parser() -> Generator[Parser, str, RECORD_TYPE]:
    """ See ``_record_parser()`` """
    return _record_parser()


@generate('ADIF FILE')
def file_parser() -> Generator[Parser, str, FILE_TYPE]:
    """ See ``_file_parser()`` """
    return _file_parser()
