# Copyright (c) 2019 Antony Robert Jordan

"""
A parser for ADIF2 files.
"""

import ADIF.parser


def parse_file(path: str) -> ADIF.parser.FILE_TYPE:
    """
    Reads an ADIF2 file and parses it into a dictionary.

    Args:
        path: The path to the file to read.

    Returns:
        A dictionary representation of the ADIF file:

        The "header" key contains a tuple with the text from the
        beginning of the file and a dictionary of any fields in the
        header.

        The "records" key contains a list of record dictionaries.
    """
    f = open(path, 'r')
    data = ADIF.parser.file_parser(f.read())
    f.close()

    return data
