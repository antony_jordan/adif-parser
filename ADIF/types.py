# Copyright (c) 2019 Antony Robert Jordan

"""
Provides functions for converting ADIF2 ASCII representations of data
into their Pythonic equivalents.
"""

from datetime import datetime, date, time
from typing import Union, Dict, Callable

########################################################################################################################
# Type aliases to make type hinting readable
########################################################################################################################

NUMBER_TYPE = Union[float, int]
ADIF_TYPES = Union[str, bool, NUMBER_TYPE, date, time]

########################################################################################################################
# Type Conversion Functions
########################################################################################################################


def to_number(data: str) -> NUMBER_TYPE:
    """
    Converts and ADIF2 ASCII string into a number type.

    Args:
        data: The ASCII data to convert.
              Must consist of numeric characters and
              the ``.`` character only.

    Returns:
        A float or integer reprisentation of the input number.

    Examples:
        >>> to_number('4.2')
        4.2

        >>> type(to_number('4.2'))
        <class 'float'>

        >>> to_number('42')
        42

        >>> type(to_number('42'))
        <class 'int'>

        >>> type(to_number('fortytwo'))
        Traceback (most recent call last):
        ...
        ValueError: from_adif_number() only accepts numeric characters in a string.
    """
    try:
        return float(data) if '.' in data else int(data)
    except ValueError:
        raise ValueError('from_adif_number() only accepts numeric characters in a string.')


def to_bool(data: str) -> bool:
    """
    Converts an ADIF2 ASCII string into a boolean.

    Args:
        data: The ASCII data to convert. Must consist of the characters
              ``Y`` or ``N`` (case insensitive).

    Returns:
        - ``True`` if input is ``Y``
        - ``False`` if input is ``N``

    Examples:

        >>> to_bool('Y')
        True

        >>> to_bool('N')
        False

        >>> to_bool('y')
        True

        >>> to_bool('n')
        False

        >>> to_bool('yes')
        Traceback (most recent call last):
        ...
        ValueError: from_adif_bool() expects "Y" or "N"!

        >>> to_bool('no')
        Traceback (most recent call last):
        ...
        ValueError: from_adif_bool() expects "Y" or "N"!
    """
    if data.upper() == 'Y':
        return True
    elif data.upper() == 'N':
        return False
    else:
        raise ValueError('from_adif_bool() expects "Y" or "N"!')


def to_date(data: str) -> date:
    """
    Converts an ADIF2 ASCII date into a ``datetime.date`` object.

    Args:
        data: The ASCII data to convert. Must be numerical data in the
              form of YYYYMMDD.

    Returns:
        A ``datetime.date`` object with the parsed date data.

    Examples:

        >>> to_date('20110629')
        datetime.date(2011, 6, 29)

        >>> to_date('29th of June 2011')
        Traceback (most recent call last):
        ...
        ValueError: time data '29th of June 2011' does not match format '%Y%m%d'
    """
    return datetime.strptime(data, '%Y%m%d').date()


def to_time(data: str) -> time:
    """
    Converts an ADIF2 ASCII time into a ``datetime.time`` object.

    Args:
        data: The ASCII data to convert. Must be numerical data in the
              form of HHMMSS.

    Returns:
        A ``datetime.time`` object with the parsed time data.

    Examples:

        >>> to_time('112233')
        datetime.time(11, 22, 33)

        >>> to_time("Twelve O'Clock")
        Traceback (most recent call last):
        ...
        ValueError: time data "Twelve O'Clock" does not match format '%H%M%S'
    """
    return datetime.strptime(data, '%H%M%S').time()


#: ADIF2 types and their callable conversion function.
DATATYPES: Dict[str, Callable] = {
    'A': str,
    'B': to_bool,
    'N': to_number,
    'S': str,
    'D': to_date,
    'T': to_time,
    'M': str,
    'L': str
}


#: Common ADIF2 tokens and their associated ADIF2 types.
TOKEN_DATATYPES: Dict[str, str] = {
    'ADDRESS': 'M',
    'ADIF_VER': 'S',
    'AGE': 'N',
    'A_INDEX': 'N',
    'ANT_AZ': 'N',
    'ANT_EL': 'N',
    'CALL': 'S',
    'CHECK': 'S',
    'CLASS': 'S',
    'COMMENT': 'S',
    'CONTACTED_OP': 'S',
    'CONTEST_ID': 'S',
    'COUNTRY': 'S',
    'CQZ': 'N',
    'CREDIT_SUBMITTED': 'A',
    'CREDIT_GRANTED': 'A',
    'DISTANCE': 'N',
    'EMAIL': 'S',
    'EQ_CALL': 'S',
    'EQSL_QSLRDATE': 'D',
    'EQSL_QSLSDATE': 'D',
    'FORCE_INIT': 'B',
    'FREQ': 'N',
    'FREQ_RX': 'N',
    'GRIDSQUARE': 'S',
    'GUEST_OP': 'S',
    'IOTA': 'S',
    'IOTA_ISLAND_ID': 'S',
    'ITUZ': 'N',
    'K_INDEX': 'N',
    'LAT': 'L',
    'LON': 'L',
    'LOTW_QSLRDATE': 'D',
    'LOTW_QSLSDATE': 'D',
    'MAX_BURSTS': 'N',
    'MS_SHOWER': 'S',
    'MY_CITY': 'S',
    'MY_CQ_ZONE': 'N',
    'MY_GRIDSQUARE': 'S',
    'MY_IOTA': 'S',
    'MY_IOTA_ISLAND_ID': 'S',
    'MY_ITU_ZONE': 'N',
    'MY_LAT': 'L',
    'MY_LON': 'L',
    'MY_NAME': 'S',
    'MY_POSTAL_CODE': 'S',
    'MY_RIG': 'S',
    'MY_SIG': 'S',
    'MY_SIG_INFO': 'S',
    'MY_STREET': 'S',
    'NAME': 'S',
    'NOTES': 'M',
    'NR_BURSTS': 'N',
    'NR_PINGS': 'N',
    'OPERATOR': 'S',
    'OWNER_CALLSIGN': 'S',
    'PFX': 'S',
    'PRECEDENCE': 'S',
    'PROGRAMID': 'S',
    'PROGRAMVERSION': 'S',
    'PUBLIC_KEY': 'S',
    'QSLMSG': 'M',
    'QSLRDATE': 'D',
    'QSLSDATE': 'D',
    'QSL_VIA': 'S',
    'QSO_DATE': 'D',
    'QSO_DATE_OFF': 'D',
    'QSO_RANDOM': 'B',
    'QTH': 'S',
    'RIG': 'M',
    'RST_RCVD': 'S',
    'RST_SENT': 'S',
    'RX_PWR': 'N',
    'SAT_MODE': 'S',
    'SAT_NAME': 'S',
    'SFI': 'N',
    'SIG': 'S',
    'SIG_INFO': 'S',
    'SRX': 'N',
    'SRX_STRING': 'S',
    'STATION_CALLSIGN': 'S',
    'STX': 'N',
    'SWL': 'B',
    'TEN_TEN': 'N',
    'TIME_OFF': 'T',
    'TIME_ON': 'T',
    'TX_PWR': 'N',
    'USERDEFn': 'S',
    'VE_PROV': 'S',
    'WEB': 'S'
}

########################################################################################################################
# Type and Token conversion Functions
########################################################################################################################


def from_type(datatype: str, data: str) -> ADIF_TYPES:
    """
    Converts from a specified ADIF2 data type to the Pythonic
    equivalent.

    Args:
        datatype: The ADIF2 data type to convert from.
                  See ``DATATYPES`` for a list.
        data: The ASCII data to convert.

    Returns:
        A python object of the correct type representing the
        input data and type.

    Examples:

        >>> from_type('A', 'test string')
        'test string'

        >>> from_type('B', 'Y')
        True

        >>> from_type('B', 'y')
        True

        >>> from_type('B', 'N')
        False

        >>> from_type('B', 'n')
        False

        >>> from_type('N', '42')
        42

        >>> from_type('N', '4.2')
        4.2

        >>> from_type('S', 'test string')
        'test string'

        >>> from_type('D', '20110629')
        datetime.date(2011, 6, 29)

        >>> from_type('T', '112233')
        datetime.time(11, 22, 33)

        >>> from_type('M', 'test\\nstring')
        'test\\nstring'

        >>> from_type('L', 'test string')
        'test string'

        >>> from_type('X', 'test string')
        Traceback (most recent call last):
        ...
        ValueError: convert_to_datatype() expects type to be one of A, B, N, S, D, T, M, L
    """
    if datatype.upper() in DATATYPES.keys():
        return DATATYPES[datatype.upper()](data)
    else:
        raise ValueError('convert_to_datatype() expects type to be one of {}'.format(', '.join(DATATYPES.keys())))


def from_token(token: str, data: str) -> ADIF_TYPES:
    """
    Converts from a specified ADIF2 data type to the Pythonic
    equivalent.

    Args:
        token: The ADIF2 token to derive the datatype from.
                  See ``TOKEN_DATATYPES`` for a list.
        data: The ASCII data to convert.

    Returns:
        A python object of the correct type representing the
        input data and type based on the input token.
    """
    if token.upper() in TOKEN_DATATYPES.keys():
        return from_type(TOKEN_DATATYPES[token.upper()], data)
    else:
        return str(data)
